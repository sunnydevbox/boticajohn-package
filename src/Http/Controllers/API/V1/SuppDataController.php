<?php
namespace Sunnydevbox\Boticajohn\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\Controller;
use Dingo\Api\Http\Request;

class SuppDataController extends Controller
{
    public function index(Request $request)
    {
        return $this->service->_suppData($request);
    }

    public function __construct(
        \Sunnydevbox\Boticajohn\Services\SupplementDataService $supplementDataService
    ) {
        $this->service = $supplementDataService;
    }
}