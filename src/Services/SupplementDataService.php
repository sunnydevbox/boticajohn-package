<?php
namespace Sunnydevbox\Boticajohn\Services;

use Sunnydevbox\TWCore\Services\SupplementDataService as SuppdataService;

class SupplementDataService extends SuppdataService
{
    public function d($d)
    {
        switch($d) {
            case 'roles':
                return $this->rpoRole->all()->toArray();
            break;

            case 'permissions':
                return $this->rpoPaymerpoPermissionntTerm->all()->toArray();
            break;

            case 'categories':
                return $this->rpoCategory->all()->toArray();
            break;
            
            case 'delivery_methods':
            case 'delivery-methods':
                return $this->rpoDeliveryMethod->all()->toArray();
            break;

            case 'units':
            case 'metrics':
                return $this->rpoMetric->all()->toArray();
            break;

            case 'locations':
                return $this->rpoLocation->all()->toArray();
            break;

            case 'manufacturers':
                return $this->rpoManufacturer->all()->toArray();
            break;

            case 'suppliers':
                return $this->rpoSupplier->all()->toArray();
            break;

            case 'payment_terms':
            case 'payment-terms':
                return $this->rpoPaymentTerm->all()->toArray();
            break;


            default:
                return [];
            break;
        }
    }

    public function __construct(
        \Sunnydevbox\TWUser\Repositories\Role\RoleRepository $rpoRole,
        \Sunnydevbox\TWUser\Repositories\Permission\PermissionRepository $rpoPermission,
        
        \Sunnydevbox\TWInventory\Repositories\Supplier\SupplierRepository $rpoSupplier,
        \Sunnydevbox\TWInventory\Repositories\Category\CategoryRepository $rpoCategory,
        \Sunnydevbox\TWInventory\Repositories\DeliveryMethod\DeliveryMethodRepository $rpoDeliveryMethod,
        \Sunnydevbox\TWInventory\Repositories\Location\LocationRepository $rpoLocation,
        \Sunnydevbox\TWInventory\Repositories\Manufacturer\ManufacturerRepository $rpoManufacturer,
        \Sunnydevbox\TWInventory\Repositories\Metric\MetricRepository $rpoMetric,
        \Sunnydevbox\TWInventory\Repositories\PaymentTerm\PaymentTermRepository $rpoPaymentTerm
        


    ) {
        $this->rpoRole = $rpoRole;
        $this->rpoPermission = $rpoPermission;
        $this->rpoSupplier = $rpoSupplier;
        $this->rpoCategory = $rpoCategory;
        $this->rpoDeliveryMethod = $rpoDeliveryMethod;
        $this->rpoMetric = $rpoMetric;
        $this->rpoLocation = $rpoLocation;
        $this->rpoManufacturer = $rpoManufacturer;
        $this->rpoPaymentTerm = $rpoPaymentTerm;


    }
}