<?php
namespace Sunnydevbox\Boticajohn\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Sunnydevbox\TWUser\Models\User as TWUserModel;
use \Sunnydevbox\TWCore\Repositories\TWMetaTrait;

class Branch extends BaseModel
{
    use TWMetaTrait;

    public $timestamps = false;

    protected $meta = [
        'first_name',
        'middle_name',
        'last_name',
        'phone',
        'address_1',
        'city',
        'state',
        'zipcode',
    ];



    public function getTable()
    {
        return config('boticajohn.tables.branches');
    }
}