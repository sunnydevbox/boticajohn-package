<?php
namespace Sunnydevbox\Boticajohn\Models;

use \Sunnydevbox\TWUser\Models\User as TWUserModel;
use \Sunnydevbox\TWCore\Repositories\TWMetaTrait;

class User extends TWUserModel
{
    use TWMetaTrait;

    protected $appends = [
        'first_name', 
        'last_name',
    ];

    protected $meta = [
        'first_name',
        'middle_name',
        'last_name',
        'phone',
        'address_1',
        'city',
        'state',
        'zipcode',
    ];

    public function getFirstNameAttribute()
    {
        return $this->getMeta('first_name');
    }

    public function getLastNameAttribute()
    {
        return $this->getMeta('last_name');
    }

}