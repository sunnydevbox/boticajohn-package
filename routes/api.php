<?php


if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider')) {


	$api = app('Dingo\Api\Routing\Router');

	$api->version('v1', ['middleware' => ['api.auth']], function ($api) {
		$api->get('suppdata', '\Sunnydevbox\Boticajohn\Http\Controllers\API\V1\SuppDataController@index');

		$api->group(['prefix' => 'inventory'], function($api) {
			$api->resource('branches', 		config('boticajohn.controllers.branch'));
		});

	});
}